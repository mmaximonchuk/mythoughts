console.log('ready!');
const navBtn = document.querySelector('.nav__btn');
const menuNav = document.querySelector('.nav__menu');
const headerNav = document.querySelector('.header-nav');
const athorization = document.querySelector('.athorization');
navBtn.onclick = () => {
  console.log('click!');
  navBtn.classList.toggle('nav__btn--close');
  menuNav.classList.toggle('nav__menu--visible');
  headerNav.classList.toggle('header-nav--visible');
  athorization.classList.toggle('athorization--visible');
};
