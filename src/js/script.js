const menuBtn = document.querySelector('.burger');
const header = document.querySelector('.header');
const nav = document.querySelector('.nav');
const burger = document.querySelector('.burger');
const openCloseIcon = document.querySelector('#open-close-icon');

let checker = true;

function disableScrolling() {
    const x=window.scrollX;
    const y=window.scrollY;
    window.onscroll= () => window.scrollTo(x, y);
}

function enableScrolling() {
    window.onscroll= () => {};
}

menuBtn.onclick = () => {
    header.classList.toggle('header--opened');
    nav.classList.toggle('nav--opened');
    burger.classList.toggle('burger--opened');
    if(checker) {
        disableScrolling();
        checker=!checker;
    } else {
        enableScrolling();
        checker=!checker;
    } 
    console.log(checker);
}

